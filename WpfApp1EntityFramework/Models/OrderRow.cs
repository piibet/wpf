﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1EntityFramework.Models
{
    public class OrderRow
    {
        public int Id { get; set; }
        public float Discount { get; set; }

        public int OrderId { get; set; }  //FK
        public Order Order { get; set; }  // Objekti referenssi

        public int ProductId { get; set; }  //FK
        public Product Product { get; set; }  // Objekti referenssi
    }
}
