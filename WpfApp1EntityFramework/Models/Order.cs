﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1EntityFramework.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Ordered { get; set; }
        public int Status { get; set; }

        public int CustomerId { get; set; }
        public Customer Customer { get; set; }  // Objekti referenssi

        public List<OrderRow> OrderRows { get; set; }
    }
}
