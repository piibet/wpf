﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApp1EntityFramework.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace WpfApp1EntityFramework
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            InitDb();
        }

        private void InitDb()
        {
            Customer customer = new Customer()
            {
                Id = 1,
                 Name = "Testi asiakas",
                Address = "Opistotie2M"
            };

            Order order = new Order()
            {
                Id = 1,
                Name = "Eka orderi",
                Status = 2,
                Ordered = DateTime.Now,

                CustomerId = customer.Id,
                Customer = customer
            };

            Product product = new Product()
            {
                Id = 100,
                Name = "Hieno tuote",
                Price = 99.0f
            };

            OrderRow orderRow = new OrderRow()
            {
                Id = 1,
                Discount = 0.0f,

                OrderId = order.Id,

                ProductId = product.Id,
                Product = product,
            };

            OrdersContext db = new OrdersContext();
            if (db.Customers.Count() == 0)
            {
                db.Customers.Add(customer);
                db.Orders.Add(order);
                db.Products.Add(product);
                db.OrderRow.Add(orderRow);
                db.SaveChanges();
            }

        }

        private void ShowDatabase_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new OrdersContext())
            {
                var showOrders = db.Orders
                    .Include(order => order.Customer)
                    .Include(order => order.OrderRows)
                    .ToList();          
                dataGrid1.ItemsSource = showOrders; //näyttää kaikki tilaukset


            }

        }

        private void dataGrid1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            using (var db = new OrdersContext())
            {
                var showProducts = db.Products
                    .Include(product => product.OrderRows)
                    .ToList();
                dataGrid2.ItemsSource = showProducts; //näyttää tuotteet

                var showCustomer = db.Customers
                     .Include(customer => customer.Order)
                    .ToList();
                dataGrid3.ItemsSource = showCustomer; //näyttää asiakkaat

            }
        }
    }
}
